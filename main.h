#ifndef MAIN_H_
#define MAIN_H_

/* HEADER FILE - MAIN.H - SpellChecker - Completed by Michael Adeyinfa */

/* Creation of struct node, to hold key value and the array of character containing 
 a dictionary word.
*/
typedef struct node {
    int key;
    char word[100];
    struct node *next;
} Node;

/* 
	Creation of struct hash, to determine where the head node is in memory.
*/
typedef struct hash {
    struct node *head;
    int count;
} Hash;

/*
	Creating the new node where space is allocated in memory as well.
	Returns a node struct.
*/
Node * createNode(int key, char *word);

/*
	Inserting into the hash table using key and word inputted.
*/
void insertToHash(int key, char *word);

/*
	Deletes node from hash table using the key value derived from struct node.
*/
void deleteFromHash(int key);

/*
	Search function for hash table, using the node key to search for the particular word,
	associated to the key.
*/
void searchInHash(int key);

/*
	Prints or displays the Hash table, with the keys and words currently in the dictionary 
	hashmap/hash table.
*/
void display();

/**
	https://stackoverflow.com/questions/1726302/removing-spaces-from-a-string-in-c
	This function removes any spaces and new lines in
	the string passed from the parameter.
*/
void removeSpaces(char* source);

/** 
	This function loads the file into the dictionary.
	It will insert a new node for every row in the file.
	User will have to input the key value.
*/
void loadHash(char *loadFileName, int key);

/*
	Saves the hashmap/hashtable by accepting a char pointer [filename] and 
	while each Node in the hashmap is not null, saves the word attribute of 
	the Node into the specified file. 
*/
void saveHash(char *outputFileName);

/*
	This function spellChecks a file passed from the parameter.
	It will compare the file with the dictionary. The correct 
	number of word to be stored is needed. If a string is not
	found then it is assumed to be a spelling mistake and will
	be reported to the user. The number of correct and incorrect
	will be displayed in the end.
*/
void spellCheck(char *loadFileName);


#endif //MAIN_H_


