#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "main.h"
struct hash *hashTable = NULL;
int countOfEle = 0;
 
 /**/
Node * createNode(int key, char *word) {
    struct node *newnode;
    newnode = (struct node *) malloc(sizeof(struct node));
    newnode->key = key;
    strcpy(newnode->word, word);
    newnode->next = NULL;
    return newnode;
}
 
/*
	Inserting into the hash table using key and word inputted.
*/
void insertToHash(int key, char *word) {
    int hashIndex = key % countOfEle;
    struct node *newnode = createNode(key, word);
    /* head of list for the node with index "hashIndex" */
    if (!hashTable[hashIndex].head) {
        hashTable[hashIndex].head = newnode;
        hashTable[hashIndex].count = 1;
        return;
    }
    /* adding new node to the list */
    newnode->next = (hashTable[hashIndex].head);
    /*
     * update the head of the list and no of
     * nodes in the current bucket
     */
    hashTable[hashIndex].head = newnode;
    hashTable[hashIndex].count++;
    return;
}

/*
	Delete node from hash table using the key value.
*/
 
void deleteFromHash(int key) {
    /* find the node using hash index */
    int hashIndex = key % countOfEle, flag = 0;
    struct node *temp, *myNode;
    /* get the list head from current bucket */
    myNode = hashTable[hashIndex].head;
    if (!myNode) {
        printf("Given data is not present in hash Table!!\n");
        return;
    }
    temp = myNode;
    while (myNode != NULL) {
        /* delete the node with given key */
        if (myNode->key == key) {
            flag = 1;
            if (myNode == hashTable[hashIndex].head)
                hashTable[hashIndex].head = myNode->next;
            else
                temp->next = myNode->next;
 
            hashTable[hashIndex].count--;
            free(myNode);
            break;
        }
        temp = myNode;
        myNode = myNode->next;
    }
    if (flag)
        printf("Data deleted successfully from Hash Table\n");
    else
        printf("Given data is not present in hash Table!!!!\n");
    return;
}

/*
	Search function for hash table.
*/
 
void searchInHash(int key) {
    int hashIndex = key % countOfEle, flag = 0;
    struct node *myNode;
    myNode = hashTable[hashIndex].head;
    if (!myNode) {
        printf("Search element unavailable in hash table\n");
        return;
    }
    while (myNode != NULL) {
        if (myNode->key == key) {
            printf("Key  : %d\n", myNode->key);
            printf("Word     : %s\n", myNode->word);
            flag = 1;
            break;
        }
        myNode = myNode->next;
    }
    if (!flag)
        printf("Search element unavailable in hash table\n");
    return;
}

/**
	This function prints the hash table onto
	the console window for user interactions.
*/
void display() {
    struct node *myNode;
    int i;
    for (i = 0; i < countOfEle; i++) {
        if (hashTable[i].count == 0)
            continue;
        myNode = hashTable[i].head;
        if (!myNode)
            continue;
        printf("\nData at index %d in Hash Table:\n", i);
        printf("Key     Word          \n");
        printf("-----------------------\n");
        while (myNode != NULL) {
            printf("%-12d", myNode->key);
            printf("%s\n", myNode->word);
            myNode = myNode->next;
        }
    }
    return;
}

/**
	https://stackoverflow.com/questions/1726302/removing-spaces-from-a-string-in-c
	This function removes any spaces and new lines in
	the string passed from the parameter.
*/
void removeSpaces(char* source)
{
  char* i = source;
  char* j = source;
  while(*j != 0)
  {
    *i = *j++;
    if(*i != ' ' && *i != '\n')
      i++;
  }
  *i = 0;
}

/** 
	This function loads the file into the dictionary.
	It will insert a new node for every row in the file.
	User will have to input the key value.
*/
void loadHash(char *loadFileName, int key) {
	FILE *ofp;
	ofp = fopen(loadFileName, "r");
	if (ofp == NULL)
	{
		fprintf(stderr, "Can't open output file %s!\n",
			loadFileName);
		exit(1);
	}
	char *buffer= malloc(sizeof(*buffer) * 1024);

	// Storing file into buffer until EoF
	while(fgets(buffer, 1024, ofp) != NULL){	
		removeSpaces(buffer);
		// Insert to hash for each line from buffer.
		insertToHash(key,buffer);
	}
	fclose(ofp);
	
	printf("Data loaded successfully into hash table.");
	
}

/**
	This function saves the dictionary onto the file.
*/
void saveHash(char *outputFileName) {
	FILE *ofp;
	ofp = fopen(outputFileName, "w");
	if (ofp == NULL)
	{
		fprintf(stderr, "Can't open output file %s!\n",
			outputFileName);
		exit(1);
	}
	struct node *myNode;
	int i;
	for (i=0; i< countOfEle; i++) 
	{
		if(hashTable[i].count == 0)
			continue;
		myNode = hashTable[i].head;
		if(!myNode)
			continue;
		while(myNode != NULL)
		{
			fprintf(ofp, "%s\n", myNode->word);
			myNode = myNode->next;
		}
	} 
	fclose(ofp);
}

/*
	This function spellChecks a file passed from the parameter.
	It will compare the file with the dictionary. The correct 
	number of word to be stored is needed. If a string is not
	found then it is assumed to be a spelling mistake and will
	be reported to the user. The number of correct and incorrect
	will be displayed in the end.
*/
void spellCheck(char *loadFileName){
	FILE *ofp;
	ofp = fopen(loadFileName, "r");
	if (ofp == NULL)
	{
		fprintf(stderr, "Can't open output file %s!\n",
			loadFileName);
		exit(1);
	}

	char *buffer= malloc(sizeof(*buffer) * 1024);
	struct node *myNode;
	char *word;
	int correctWord = 0;
	int incorrectWord = 0;
	int lineCount = 1;
	int correctFlag;
	int count = 0;
	int compare = 0;

	// Storing file into buffer until EoF
	while(fgets(buffer, 1024, ofp) != NULL){	
		int i;
	    for (i = 0; i < countOfEle; i++) {
			compare = 0;
			correctFlag = 0;
			count = 0;

	        if (hashTable[i].count == 0)
	            continue;
	        myNode = hashTable[i].head;
	        if (!myNode)
	            continue;
	        while (myNode != NULL) {
				word = myNode->word;
				removeSpaces(buffer);

				// Store return value of compare
				compare = strcmp(word, buffer);

				// If the string is equal to each other
				if(compare == 0){
					correctFlag = 1;
					
					correctWord++;

				} 

				// If correctFlag is false
				if (correctFlag == 0) {
					++count;
					// If the last element is still incorrect
					if(count == countOfEle){
						printf("\nLine %d, %s is incorrect.", lineCount, buffer);
						incorrectWord++;
					}
				}				
				
				// Next Node
	            myNode = myNode->next;
	        }
	    }

		// Count the line it is on in the file
		lineCount++;	
	}
	printf("\n\nNumber of correct words: %d", correctWord);
	printf("\nNumber of incorrect words: %d\n", incorrectWord);
	fclose(ofp);
}

int main(int argc, char *argv[]) {
    int n, ch, key;
    char word[100];
	char outputFileName[] = "dictionary.txt";
    printf("Enter the number of words to be stored:");
    scanf("%d", &n);
    countOfEle = n;
    /* create hash table with "n" no of buckets */
    hashTable = (struct hash *) calloc(n, sizeof(struct hash));
    while (1) {
        printf("\n1. Insert word\t\t2. Delete word\n");
        printf("3. Search for word\t4. Display dictionary\n");
  		printf("5. Load dictionary\t6. Save dictionary\n");    
		printf("7. SpellCheck\t\t8. Exit\n");
		printf("Enter your choice:");
        scanf("%d", &ch);
        switch (ch) {
        case 1:
            printf("Enter the key value:");
            scanf("%d", &key);
            getchar();
            printf("Word:");
            fgets(word, 100, stdin);
            word[strlen(word) - 1] = '\0';
            /*inserting new node to hash table */
            insertToHash(key, word);
            break;
 
        case 2:
            printf("Enter the key to perform deletion:");
            scanf("%d", &key);
            /* delete node with "key" from hash table */
            deleteFromHash(key);
            break;
 
        case 3:
            printf("Enter the key to search:");
            scanf("%d", &key);
			/* search for node with "key" from hash table*/
            searchInHash(key);
            break;
        case 4:
            display();
            break;
		case 5:
			printf("Enter the key value:");
            scanf("%d", &key);
			loadHash(outputFileName, key);
			break;
		case 6:
			saveHash(outputFileName);
			printf("Saved dictionary successfully!\n");
			break;
		case 7:
			spellCheck(outputFileName);
			break;
        case 8:
            exit(0);
        default:
            printf("U have entered wrong option!!\n");
            break;
        }
    }
    return 0;
}
